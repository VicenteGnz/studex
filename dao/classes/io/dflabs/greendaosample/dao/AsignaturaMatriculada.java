package io.dflabs.greendaosample.dao;

// THIS CODE IS GENERATED BY greenDAO, EDIT ONLY INSIDE THE "KEEP"-SECTIONS

// KEEP INCLUDES - put your custom includes here
import android.os.Parcel;
import android.os.Parcelable;
// KEEP INCLUDES END
/**
 * Entity mapped to table ASIGNATURA_MATRICULADA.
 */
public class AsignaturaMatriculada {

    private Long id;
    private String nombre_asignatura;
    private Integer p_teoria;
    private Integer p_practica;
    private Integer p_actividades;

    // KEEP FIELDS - put your custom fields here
    private int foto;
    private String linkProgramaAsignatura;
    // KEEP FIELDS END

    public AsignaturaMatriculada() {
    }

    public AsignaturaMatriculada(Long id) {
        this.id = id;
    }

    public AsignaturaMatriculada(Long id, String nombre_asignatura, Integer p_teoria, Integer p_practica, Integer p_actividades) {
        this.id = id;
        this.nombre_asignatura = nombre_asignatura;
        this.p_teoria = p_teoria;
        this.p_practica = p_practica;
        this.p_actividades = p_actividades;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre_asignatura() {
        return nombre_asignatura;
    }

    public void setNombre_asignatura(String nombre_asignatura) {
        this.nombre_asignatura = nombre_asignatura;
    }

    public Integer getP_teoria() {
        return p_teoria;
    }

    public void setP_teoria(Integer p_teoria) {
        this.p_teoria = p_teoria;
    }

    public Integer getP_practica() {
        return p_practica;
    }

    public void setP_practica(Integer p_practica) {
        this.p_practica = p_practica;
    }

    public Integer getP_actividades() {
        return p_actividades;
    }

    public void setP_actividades(Integer p_actividades) {
        this.p_actividades = p_actividades;
    }

    // KEEP METHODS - put your custom methods here
    public int getFoto() {
        return foto;
    }

    public void setFoto(int foto) {
        this.foto = foto;
    }

    public String getLinkProgramaAsignatura() {
        return linkProgramaAsignatura;
    }

    public void setLinkProgramaAsignatura(String linkProgramaAsignatura) {
        this.linkProgramaAsignatura = linkProgramaAsignatura;
    }
    // KEEP METHODS END

}
