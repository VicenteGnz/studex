package io.dflabs.greendaosample.dao;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

/**
 * Created by Vicente on 26/11/2016.
 */

public class CustomDaoMaster extends DaoMaster {
    public CustomDaoMaster(SQLiteDatabase db) {
        super(db);
    }

    public static class OpenHelper extends DaoMaster.OpenHelper {
        public OpenHelper(Context context, String name, SQLiteDatabase.CursorFactory factory) {
            super(context, name, factory);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            super.onCreate(db);
            String[] Asignaturas={"Desarrollo de Programas","Analisis y Diseño de Algoritmos","Programación en Internet","Teoría de Lenguajes",
                    "Arquitecturas Software para Entornos Empresariales","Diseño e Interacción de Sistemas Inteligentes","Fisica","Calculo","Algebra"};
            int i=0;
            while(i<Asignaturas.length){
                db.execSQL("INSERT INTO Asignatura VALUES ("+i+",'" + Asignaturas[i]+"')");
                i++;
            }

        }
        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            Log.i("greenDAO", "Upgrading schema from version " + oldVersion + " to " + newVersion + " by dropping all tables");
            dropAllTables(db, true);
            onCreate(db);
        }
    }
}
