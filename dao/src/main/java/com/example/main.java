package com.example;

import  de.greenrobot.daogenerator.Entity;
import  de.greenrobot.daogenerator.Schema;
import  de.greenrobot.daogenerator.DaoGenerator;
public class main {

    public static void main(String[] args) throws Exception{

        Schema schema =new Schema(1,"io.dflabs.greendaosample.dao");
        schema.enableKeepSectionsByDefault();
        createDatabase(schema);
        DaoGenerator generator=new DaoGenerator();
        generator.generateAll(schema,args[0]);
    }

    private static void createDatabase(Schema schema){
        Entity post =schema.addEntity("Asignatura");
        post.addIdProperty();
        post.addStringProperty("nombre_asignatura");


        Entity post1= schema.addEntity("AsignaturaMatriculada");
        post1.addIdProperty();
        post1.addStringProperty("nombre_asignatura");
        post1.addIntProperty("p_teoria");
        post1.addIntProperty("p_practica");
        post1.addIntProperty("p_actividades");

        Entity post2= schema.addEntity("Tarea");
        post2.addLongProperty("id_tarea");
        post2.addStringProperty("nombre_tarea").primaryKey();
        post2.addStringProperty("tipo_tarea");
        post2.addFloatProperty("nota_tarea");

        Entity post3= schema.addEntity("Evento");
        post3.addStringProperty("Descripcion_evento").primaryKey();
        post3.addStringProperty("fecha_evento").notNull();

    }

}
