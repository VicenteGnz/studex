package com.example.vicente.studex_v1;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import de.greenrobot.dao.query.Query;
import io.dflabs.greendaosample.dao.AsignaturaMatriculada;
import io.dflabs.greendaosample.dao.AsignaturaMatriculadaDao;
import io.dflabs.greendaosample.dao.Tarea;
import io.dflabs.greendaosample.dao.TareaDao;

import static com.example.vicente.studex_v1.Asignatura_main.PlaceholderFragment.ids;

public class Asignatura_main extends AppCompatActivity {
    /**
     *
     * Atributos de la clase Asignatura_main
     */
    private boolean mModoAhorro =false;
    private SharedPreferences mPreferences;
    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    private SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPreferences= PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        mModoAhorro =mPreferences.getBoolean("switch_preference_2",false);

        if(mModoAhorro)
            setTheme(R.style.AppThemeDark);
        else
            setTheme(R.style.AppTheme);

        setContentView(R.layout.activity_asignatura_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager(),this);

        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

    }


    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {
        protected List<Tarea> tareas;
        protected static long[] ids;
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";

        public PlaceholderFragment() {
        }

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public  static PlaceholderFragment newInstance(int sectionNumber,Context con) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            if(ids!=null){

                List<AsignaturaMatriculada>  asignaturaMatriculada= DaoApp.getAsigMatDao().queryBuilder().where(AsignaturaMatriculadaDao.Properties.Id.eq(ids[sectionNumber])).list();

                Bundle args = new Bundle();
                args.putInt(fragment.ARG_SECTION_NUMBER,  sectionNumber);
                args.putLong("id",ids[sectionNumber]);
                args.putString("nombre",asignaturaMatriculada.get(0).getNombre_asignatura());
                args.putLong("idPaginaActual",asignaturaMatriculada.get(0).getId());
                fragment.setArguments(args);
            }

            return fragment;
        }
        private boolean existeTarea(String Nombre){
            Query Existe = DaoApp.getTareaDao().queryBuilder().where(TareaDao.Properties.Nombre_tarea.eq(Nombre)).build();
            if(Existe!=null) {
                List<Tarea> tarea = Existe.list();
                if (tarea.size() < 1)
                    return false;
                else
                    return true;
            }else
                return false;
        }

        /**
         * Metodo que inserta una tarea en la base de datos a partir de unos datos facilitados por parametro.
         * @param id       id de la tarea
         * @param n_tarea   nombre de la tarea
         * @param t_tarea   tipo de la tarea
         * @param nota_tarea    float con la nota de la tarea
         * @return
         */
        public String insertarNota(long id,String n_tarea,String t_tarea,Float nota_tarea){
            String mensaje="";
            Tarea tarea=new Tarea(id,n_tarea,t_tarea,nota_tarea);
            tareas.add(tarea);
            AlertDialog alerta= new AlertDialog.Builder(getContext()).create();
            if(!existeTarea(tarea.getNombre_tarea())){
                DaoApp.getTareaDao().insertInTx(tarea);
                mensaje = "insertada";
                alerta.setTitle("Listo");
                alerta.setMessage("Ha insertado correctamente la nota de su tarea");
                alerta.show();
            }else {
                mensaje = "No insertada";
                alerta.setTitle("Error");
                alerta.setMessage("Comprueba que no tengas ya insertada una tarea con ese nombre");
                alerta.show();
            }
            return  mensaje;

        }

        /**
         *Metodo que muestra la media de una asignatura dada
         * @param id id de la asigantura
         */
        public void MostrarMedia(long id){
            List<Tarea> tarea=DaoApp.getTareaDao().loadAll();
            String t_tarea="";
            Float nota_tarea=0f;
            Float nota_Actividad=0f;
            Float nota_Practicas=0f;
            Float nota_Teoria=0f;
            int contadorPractica=0;
            int contadorTeoria=0;
            int contadorActividad=0;
            int i=0;
            while(i<tarea.size()){
                if(tarea.get(i).getId_tarea()==id){
                    t_tarea= tarea.get(i).getTipo_tarea();
                    nota_tarea=tarea.get(i).getNota_tarea();
                    switch (t_tarea){
                        case "Proyecto":
                            contadorPractica++;
                            nota_Practicas=(nota_tarea*0.4f)+nota_Practicas;
                            break;
                        case "Examen_Teorico":
                            contadorTeoria++;
                            nota_Teoria=(nota_tarea*0.5f)+nota_Teoria;
                            break;
                        case "Actividad":
                            contadorActividad++;
                            nota_Actividad=(nota_tarea*0.1f)+nota_Actividad;
                            break;
                        case "Examen_Practico":
                            contadorPractica++;
                            nota_Practicas=(nota_tarea*0.4f)+nota_Practicas;
                            break;
                    }}

                i++;

            }
            nota_Practicas=nota_Practicas/contadorPractica;
            nota_Actividad=nota_Actividad/contadorActividad;
            nota_Teoria=nota_Teoria/contadorTeoria;
            AlertDialog alerta= new AlertDialog.Builder(this.getContext()).create();
            alerta.setTitle("Notas");
            Float sumaTotal=nota_Actividad+nota_Practicas+nota_Teoria;

            alerta.setMessage("Aqui tienes tu media (Ponderada): \n" +
                                    "Teoria (0/5):  " + nota_Teoria+"\n"+
                                    "Practica (0/4):  " + nota_Practicas +"\n"+
                                    "Actividades (0/1):  " + nota_Actividad +"\n"+
                                    "Total (0/10):  "+sumaTotal);
            alerta.show();

        }


        @Override
        public View onCreateView(final LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            final View rootView = inflater.inflate(R.layout.fragment_asignatura_main, container, false);

            tareas=new ArrayList<>();
            TextView textTitulo= (TextView) rootView.findViewById(R.id.Nombre_Asignatura);

            final List<AsignaturaMatriculada>  Asignaturas=DaoApp.getAsigMatDao().loadAll();
            final String nombre_nuevo = getArguments() != null ? getArguments().getString("nombre") : "";
            textTitulo.setText(nombre_nuevo);
            final Long id_nuevo = getArguments() != null ? getArguments().getLong("idPaginaActual") : 0;

            Button button =(Button) rootView.findViewById(R.id.button_ficha12A);
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int i=0;
                    while(i<Asignaturas.size()) {
                        if(nombre_nuevo.equals(Asignaturas.get(i).getNombre_asignatura())){
                            Intent in = new Intent(Intent.ACTION_VIEW);
                            in.setData(Uri.parse(Asignaturas.get(i).getLinkProgramaAsignatura()));
                            startActivity(in);
                            i++;
                        }
                        i++;
                    }
                }
            });
            final RadioGroup rg=(RadioGroup) rootView.findViewById(R.id.radio_group);
                rg.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(RadioGroup group, int checkedId) {

                    }
                });
            ImageButton button2=(ImageButton) rootView.findViewById(R.id.imageButton_addNote) ;
            button2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    EditText et1 = (EditText) rootView.findViewById(R.id.text_nombreTarea);
                    EditText et2 = (EditText) rootView.findViewById(R.id.text_notaTarea);

                    if(rg.getCheckedRadioButtonId()!=-1) {
                        String Nombre= et1.getText().toString();
                        String nota= et2.getText().toString();
                        float notaInt=Float.parseFloat(nota);
                        switch (rg.getCheckedRadioButtonId()) {
                            case R.id.radioButton_examenT:
                                if ( Nombre.equals("")|| nota.equals("") || notaInt>10) {
                                    if (notaInt > 10)
                                        alertaFaltanDatos(rootView,"Inserte una nota entre 0 y 10");
                                    else
                                        alertaFaltanDatos(rootView,"¿Está seguro que ha rellenado los campos Nombre de tarea y Nota?");
                                } else {
                                    insertarNota(id_nuevo, Nombre, "Examen_Teorico", Float.parseFloat(nota));

                                }
                                break;
                            case R.id.radioButton_examenP:
                                if ( Nombre.equals("")|| nota.equals("") || notaInt>10) {
                                    if (notaInt > 10)
                                        alertaFaltanDatos(rootView,"Inserte una nota entre 0 y 10");
                                    else
                                        alertaFaltanDatos(rootView,"¿Está seguro que ha rellenado los campos Nombre de tarea y Nota?");
                                } else {

                                    insertarNota(id_nuevo, Nombre, "Examen_Practico", Float.parseFloat(nota));
                                }
                                break;
                            case R.id.radioButton_proyecto:
                                if ( Nombre.equals("")|| nota.equals("") || notaInt>10) {
                                    if (notaInt > 10)
                                        alertaFaltanDatos(rootView,"Inserte una nota entre 0 y 10");
                                    else
                                        alertaFaltanDatos(rootView,"¿Está seguro que ha rellenado los campos Nombre de tarea y Nota?");
                                } else {
                                    insertarNota(id_nuevo, Nombre, "Proyecto", Float.parseFloat(nota));
                                }
                                break;
                            case R.id.radioButton_actividad:
                                if ( Nombre.equals("")|| nota.equals("") || notaInt>10) {
                                    if (notaInt > 10)
                                        alertaFaltanDatos(rootView,"Inserte una nota entre 0 y 10");
                                    else
                                        alertaFaltanDatos(rootView,"¿Está seguro que ha rellenado los campos Nombre de tarea y Nota?");
                                } else {
                                    insertarNota(id_nuevo, Nombre, "Actividad", Float.parseFloat(nota));
                                }
                                break;
                        }

                    }else{
                        AlertDialog alerta= new AlertDialog.Builder(getContext()).create();
                        alerta.setTitle("Error");
                        alerta.setMessage("Seleciona el tipo de tarea");
                        alerta.show();
                    }


                }
            });

            Button buttonListar=(Button) rootView.findViewById((R.id.button_ListarTareas));
            buttonListar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent in=new Intent(getContext(),ListTareas.class);
                    Bundle bundle=new Bundle();
                    bundle.putLong("idActual",id_nuevo);
                    in.putExtras(bundle);
                    startActivity(in);


                }
            });
            Button button3 =(Button) rootView.findViewById(R.id.button_misNotas);
            button3.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    MostrarMedia(id_nuevo);
                }
            });

            FloatingActionButton buttonEmail=(FloatingActionButton) rootView.findViewById(R.id.button_email);
            buttonEmail.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(Intent.ACTION_VIEW);
                    Uri data = Uri.parse("mailto:?subject=" +" subject" + "&body=" + "body");
                    intent.setData(data);
                    startActivity(intent);

                }
            });

            //  textView.setText(datos);
            return rootView;
        }
        public void alertaFaltanDatos(View view,String mensaje){
            AlertDialog alerta= new AlertDialog.Builder(this.getContext()).create();
            alerta.setTitle("Error");
            alerta.setMessage(mensaje);
            alerta.show();
        }
    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        protected Context context;
        protected ArrayList<String> titulos;

        public SectionsPagerAdapter(FragmentManager fm,Context c) {
            super(fm);

            context=c;
            titulos= new ArrayList<>();
            List<AsignaturaMatriculada> asMat=DaoApp.getAsigMatDao().loadAll();
            int i=0;
            while (i<asMat.size()){
                titulos.add(asMat.get(i).getNombre_asignatura());
                i++;
            }


        }

        @Override
        public Fragment getItem(int position) {
            Bundle bundle = getIntent().getExtras();
            if(bundle.getLongArray("ids")!=null)
                ids=bundle.getLongArray("ids");
            return PlaceholderFragment.newInstance(position,getApplicationContext());
        }

        @Override
        public int getCount() {
            return DaoApp.getAsigMatDao().loadAll().size();

        }

        @Override
        public CharSequence getPageTitle(int position) {
//
            return titulos.get(position);
        }
    }
}
