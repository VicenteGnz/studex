package com.example.vicente.studex_v1;


import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.ListView;
import com.example.vicente.studex_v1.Adapters.TareasAdapter;
import java.util.List;
import io.dflabs.greendaosample.dao.Tarea;
import io.dflabs.greendaosample.dao.TareaDao;

/**
 * Clase del activity utilizado para listar las tareas de un activity en concreto
 */
public class ListTareas extends AppCompatActivity {
    /**Atributos de la clase*/
    SharedPreferences mPreferences;
    boolean mModoAhorro =false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mPreferences= PreferenceManager.getDefaultSharedPreferences(this);
        mModoAhorro =mPreferences.getBoolean("switch_preference_2",false);

        if(mModoAhorro)
            setTheme(R.style.AppThemeDark);
        else
            setTheme(R.style.AppTheme);

        setContentView(R.layout.activity_list_tareas);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Bundle bundle=getIntent().getExtras();
        long idAsignatura=bundle.getLong("idActual");

        List<Tarea> tareas=DaoApp.getTareaDao().queryBuilder().where(TareaDao.Properties.Id_tarea.eq(idAsignatura)).list();

        ListView lv =(ListView) findViewById(R.id.lista_tareas);
        TareasAdapter adapter =new TareasAdapter(this,tareas);
        lv.setAdapter(adapter);

    }


}
