package com.example.vicente.studex_v1;

import android.app.Application;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import io.dflabs.greendaosample.dao.AsignaturaDao;
import io.dflabs.greendaosample.dao.AsignaturaMatriculadaDao;
import io.dflabs.greendaosample.dao.CustomDaoMaster;
import io.dflabs.greendaosample.dao.DaoMaster;
import io.dflabs.greendaosample.dao.DaoSession;
import io.dflabs.greendaosample.dao.Evento;
import io.dflabs.greendaosample.dao.EventoDao;
import io.dflabs.greendaosample.dao.TareaDao;


public class DaoApp extends Application {


    static AsignaturaDao asigDao;
    static AsignaturaMatriculadaDao asigMatDao;
    static TareaDao tareaDao;
    static EventoDao eventoDao;
    static CustomDaoMaster.OpenHelper helper;
    static SQLiteDatabase db;

    public void onCreate(){
        super.onCreate();
        helper = new CustomDaoMaster.OpenHelper(this, "StudEx", null);
        db= helper.getWritableDatabase();
        DaoMaster daoMaster = new DaoMaster(db);
        DaoSession daoSession= daoMaster.newSession();
        asigDao =daoSession.getAsignaturaDao();
        asigMatDao=daoSession.getAsignaturaMatriculadaDao();
        tareaDao=daoSession.getTareaDao();
        eventoDao=daoSession.getEventoDao();
    }
    public static AsignaturaDao getAsigDao() {
        return asigDao;
    }

    public static boolean CheckIsDataAlreadyInDBorNot(String TableName,
                                                      String dbfield, String fieldValue) {
        String Query = "Select * from " + TableName + " where " + dbfield + " = " + fieldValue;
        Cursor cursor = db.rawQuery(Query, null);
        if(cursor.getCount() <= 0){
            cursor.close();
            return false;
        }
        cursor.close();
        return true;
    }
    public static AsignaturaMatriculadaDao getAsigMatDao() {
        return asigMatDao;
    }


    public static TareaDao getTareaDao() {
        return tareaDao;
    }

    public static EventoDao getEventoDao() {
        return eventoDao;
    }


}
