package com.example.vicente.studex_v1;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;


import com.example.vicente.studex_v1.Adapters.AdapterAsignaturas;
import com.example.vicente.studex_v1.Adapters.MyFragmentPagerAdapter;
import com.example.vicente.studex_v1.Fragments.MisAsignaturas;


public class MainActivity extends AppCompatActivity implements MisAsignaturas.Listener,AdapterAsignaturas.ListenerToDeleteSubject {

    /**Atributos de la clase principal*/
    private SharedPreferences mPreferences;
    private TabLayout mTabLayaout;
    private ViewPager mViewPager;
    private MyFragmentPagerAdapter mAdapter;
    private boolean mModoAhorro =false;
    private boolean lastPermised=false;

    @Override
    public void sendUdapte() {
        mAdapter.update();
    }
    @Override
    public void send() {
        mAdapter.update();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPreferences=PreferenceManager.getDefaultSharedPreferences(this);
        mModoAhorro =mPreferences.getBoolean("switch_preference_2",false);
        if(mModoAhorro) {
            lastPermised = true;
            setTheme(R.style.AppThemeDark);
        }
        else{
        lastPermised=false;
        setTheme(R.style.AppTheme);}

        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mTabLayaout = (TabLayout) findViewById(R.id.tab_layout);
        mViewPager = (ViewPager) findViewById(R.id.view_pager);


        mAdapter =new MyFragmentPagerAdapter(getSupportFragmentManager(),getResources().getStringArray(R.array.titles_tab));
        mViewPager.setAdapter(mAdapter);

        mTabLayaout.setupWithViewPager(mViewPager);


        getIntent().setAction("Already created");


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_settings:
                Intent i =new Intent(this,Settings.class);
                startActivity(i);
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
 }

    @Override
    public void onResume(){
        String action = getIntent().getAction();
        mModoAhorro =mPreferences.getBoolean("switch_preference_2",false);
        if((action == null || !action.equals("Already created")) && lastPermised!=mModoAhorro ) {
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
            finish();
        }
        else
            getIntent().setAction(null);
        super.onResume();

    }


}
