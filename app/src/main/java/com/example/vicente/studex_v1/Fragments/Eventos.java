package com.example.vicente.studex_v1.Fragments;

import android.Manifest;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.CalendarContract.Events;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.example.vicente.studex_v1.DaoApp;
import com.example.vicente.studex_v1.R;
import com.github.sundeepk.compactcalendarview.CompactCalendarView;
import com.github.sundeepk.compactcalendarview.domain.Event;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import de.greenrobot.dao.query.DeleteQuery;
import de.greenrobot.dao.query.QueryBuilder;
import io.dflabs.greendaosample.dao.Evento;
import io.dflabs.greendaosample.dao.EventoDao;

/**
 * Clase que gestiona el fragmen de eventos, donde se encuentra el calendario de la aplicación
 */

public class Eventos extends Fragment {


    /*** Atributos utilizados en este Fragment*/
    private SharedPreferences mPreferences;
    private View v;
    private CompactCalendarView mCompactCalendar;
    private SimpleDateFormat mDateFormatMonth;
    private Date mUltimoClickeado;
    private ImageButton mProximoMes;
    private ImageButton mAnteriorMes;
    private static final int PERMISSIONS_REQUEST_WRITE_CALENDAR = 100;
    private boolean mPermisoAjustes;
    private boolean mPermisoSistema;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        v=inflater.inflate(R.layout.eventos,container,false);

        mPreferences= PreferenceManager.getDefaultSharedPreferences(getActivity());
        mPermisoAjustes =mPreferences.getBoolean("check_box_preference_1",false);

        mCompactCalendar = (CompactCalendarView) v.findViewById(R.id.compactcalendar_view);
        mCompactCalendar.shouldScrollMonth(false);

        mDateFormatMonth = new SimpleDateFormat("MMMM - yyyy", Locale.getDefault());
        mCompactCalendar.refreshDrawableState();

        refresh();
        mPermisoSistema=false;
        if (getActivity().checkSelfPermission(Manifest.permission.WRITE_CALENDAR)
                != PackageManager.PERMISSION_GRANTED && mPermisoAjustes ==false) {

            requestPermissions(new String[]{Manifest.permission.WRITE_CALENDAR},PERMISSIONS_REQUEST_WRITE_CALENDAR

            );
        }else
            mPermisoSistema=true;
        mUltimoClickeado=mCompactCalendar.getFirstDayOfCurrentMonth();
        final TextView MonthText=(TextView) v.findViewById(R.id.textView_mesCalendario);
        MonthText.setText(mDateFormatMonth.format(new Date()));
        final Button añadirEvento=(Button) v.findViewById(R.id.button_addEvent);
        final EditText et_nombreEvento=(EditText) v.findViewById(R.id.editText_nameEvent) ;
        final Button eliminarEventos=(Button)  v.findViewById(R.id.button_eliminarEvento);
        mProximoMes=(ImageButton) v.findViewById(R.id.imageButton_MesPosterior);
        mAnteriorMes =(ImageButton) v.findViewById(R.id.imageButton_MesAnterior);

        mProximoMes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCompactCalendar.showNextMonth();
            }
        });

        mAnteriorMes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCompactCalendar.showPreviousMonth();


            }
        });

        eliminarEventos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                List<Event> listaEventos=mCompactCalendar.getEvents(mUltimoClickeado);
                int i=0;
                while(i<listaEventos.size()) {
                    QueryBuilder qb = DaoApp.getEventoDao().queryBuilder().where(EventoDao.Properties.Descripcion_evento.eq(listaEventos.get(i).getData()));
                    DeleteQuery<Evento> dq = qb.buildDelete();
                    dq.executeDeleteWithoutDetachingEntities();
                    if(mPermisoSistema)
                    deleteEventToCalendarSystem(listaEventos.get(i).getData().toString());
                    i++;
                }
                mCompactCalendar.removeEvents(mUltimoClickeado);
                mCompactCalendar.setCurrentSelectedDayBackgroundColor(Color.rgb(51,102,102));

            }
        });
        mCompactCalendar.setListener(new CompactCalendarView.CompactCalendarViewListener() {
            @Override
            public void onDayClick(Date dateClicked) {
                mUltimoClickeado=dateClicked;
                List<Event> listaEventos=mCompactCalendar.getEvents(dateClicked);
                int i=0;
                if(listaEventos.size()>0){
                    mCompactCalendar.setCurrentSelectedDayBackgroundColor(Color.rgb(51,153,204));
                }else
                    mCompactCalendar.setCurrentSelectedDayBackgroundColor(Color.rgb(51,102,102));

                while(i<listaEventos.size()){
                    Toast toast = Toast.makeText(getContext(),"Debes Realizar: "+listaEventos.get(i).getData().toString(), Toast.LENGTH_SHORT);
                    toast.show();
                    i++;
                }
            }

            @Override
            public void onMonthScroll(Date firstDayOfNewMonth) {
                MonthText.setText(mDateFormatMonth.format(firstDayOfNewMonth));
            }
        });
        añadirEvento.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String _nombre_evento=et_nombreEvento.getText().toString();
                List<Evento> eventos = DaoApp.getEventoDao().queryBuilder().where(EventoDao.Properties.Descripcion_evento.eq(_nombre_evento)).build().list();
                if(eventos.isEmpty()) {
                    DateFormat fecha_evento=new SimpleDateFormat("dd/MM/yyyy");
                    String fecha= fecha_evento.format(mUltimoClickeado);
                    long milliseconds = convertDate(fecha);
                    Evento evento1 = new Evento(_nombre_evento, fecha);
                    Event evNew = new Event(Color.BLUE, milliseconds, _nombre_evento);
                    mCompactCalendar.addEvent(evNew, true);
                    mCompactCalendar.setCurrentSelectedDayBackgroundColor(Color.rgb(51,153,204));
                    DaoApp.getEventoDao().insertInTx(evento1);
                    et_nombreEvento.setText("");
                    refresh();
                    if(mPermisoAjustes&&mPermisoSistema)
                        addEventToCalendarSystem(_nombre_evento, milliseconds);
                }

            }
        });


        return v;
    }


    /**
     * Metodo que añade un evento al calendario del sistema.
     * @param title titulo del evento a añadir
     * @param startTimeMillis comienzo del evento en milisegundos
     */
    public void addEventToCalendarSystem(String title,long startTimeMillis){
        final ContentValues event = new ContentValues();
        event.put(Events.CALENDAR_ID, 1);
        event.put(Events.TITLE, title);
        event.put(Events.DTSTART, startTimeMillis);
        event.put(Events.DTEND, startTimeMillis+60*60*1000);
        event.put(Events.ALL_DAY, 0);   // 0 for false, 1 for true
        event.put(Events.HAS_ALARM, 1); // 0 for false, 1 for true

        String timeZone = TimeZone.getDefault().getID();
        event.put(Events.EVENT_TIMEZONE, timeZone);

        Uri baseUri;
        if (Build.VERSION.SDK_INT >= 8) {
            baseUri = Uri.parse("content://com.android.calendar/events");
        } else {
            baseUri = Uri.parse("content://calendar/events");
        }
        getContext().getContentResolver().insert(baseUri, event);
    }

    /**
     * Metodo que elimina un evento al calendario del sistema.
     * @param title titulo del evento a añadir
     */
    public void deleteEventToCalendarSystem(String title) {
        Uri CALENDAR_URI = Uri.parse("content://calendar/events");
        Cursor cursors = getContext().getContentResolver().query(CALENDAR_URI, null, null, null, null);
        if(cursors!=null)
            if (cursors.moveToFirst()) {
                while (cursors.moveToNext()) {
                    String desc = cursors.getString(cursors.getColumnIndex("description"));
                    String id = cursors.getString(cursors.getColumnIndex("_id"));
                    if (desc != null)
                        if (desc.equals(title)) {
                            Uri uri = ContentUris.withAppendedId(CALENDAR_URI, Integer.parseInt(id));
                            getContext().getContentResolver().delete(uri, null, null);
                        }
                }
        }
    }
    /**
     * Metodo que permite convertir un string que contiene una fecha a milisegundos
     * @param date  fecha en formato dd/MM/yyy
     * @return
     */
    public long convertDate(String date){
        long milliseconds=0;
        SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");
        try {
            Date d = f.parse(date);
            milliseconds = d.getTime();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return milliseconds;
    }

    /**
     * Metodo que actualiza el calendario una vez insertado cualquier dato.
     */
    public void refresh(){
        List<Evento> evento = DaoApp.getEventoDao().loadAll();
        int i=0;
        String _nombre_evento="";
        String  fecha_evento="";
        long fecha=0;
        mCompactCalendar.removeAllEvents();
        while(i<evento.size()){
            _nombre_evento=evento.get(i).getDescripcion_evento();
            fecha_evento=evento.get(i).getFecha_evento();
            fecha=convertDate(fecha_evento);
            Event evNew=new Event(Color.BLUE,fecha,_nombre_evento);
            mCompactCalendar.addEvent(evNew,true);
            i++;

        }


    }
    @Override
    public void onResume() {
        super.onResume();
        mPermisoAjustes =mPreferences.getBoolean("check_box_preference_1",false);

    }

}
