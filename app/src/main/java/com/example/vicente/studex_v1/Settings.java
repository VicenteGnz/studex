package com.example.vicente.studex_v1;

/**
 * Created by Vicente on 18/11/2016.
 */
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import android.preference.PreferenceActivity;
import android.preference.PreferenceManager;


public class Settings extends PreferenceActivity implements SharedPreferences.OnSharedPreferenceChangeListener{

    SharedPreferences mPref;
    Boolean mModoAhorro =false;

    @Override
    public void onCreate(Bundle savedInstanceState) {

        mPref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        mModoAhorro = mPref.getBoolean("switch_preference_2",false);
        if(mModoAhorro)
            setTheme(R.style.AppThemeDark);
        else
            setTheme(R.style.AppTheme);

        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.settings);

    }

    @Override
    protected void onResume() {
        super.onResume();
        getPreferenceScreen().getSharedPreferences()
                .registerOnSharedPreferenceChangeListener(this);
    }
    private void restartActivity() {
        Intent intent = getIntent();
        startActivity(intent);
        finish();
    }
    @Override
    protected void onStop() {
        super.onStop();
        getPreferenceScreen().getSharedPreferences().unregisterOnSharedPreferenceChangeListener(this);
    }
    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        if(key.equals("switch_preference_2"))
            restartActivity();
    }
}
