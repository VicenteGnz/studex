package com.example.vicente.studex_v1.Fragments;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.vicente.studex_v1.DaoApp;
import com.example.vicente.studex_v1.R;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;

import java.util.ArrayList;
import java.util.List;

import io.dflabs.greendaosample.dao.AsignaturaMatriculada;
import io.dflabs.greendaosample.dao.Tarea;

/**
 * Clase del fragment de Resumen en el que se encuentra el gráfico de la aplicación.
 */

public class Resumen extends Fragment implements  OnChartValueSelectedListener{

    /**
     * Atributos de la clase Resumen
     */
    private View v;
    private BarChart mBarChart;
    private ArrayList<String> mNombreAsignaturas;
    private ArrayList<String> mNombreAsignaturasSinCortar;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.resumen, container, false);
        mBarChart = (BarChart) v.findViewById(R.id.barGraph_Asignaturas);
        reloadChart();
        return v;
    }



    /**
     * Método que recarga el gráfico
     */
    public void reloadChart() {
        List<AsignaturaMatriculada> listaAsignaturas = DaoApp.getAsigMatDao().loadAll();
        if (!listaAsignaturas.isEmpty()) {
            ArrayList<BarEntry> barEntries = new ArrayList<>();
            int i = 0;
            while (i < listaAsignaturas.size()) {
                barEntries.add(new BarEntry(MediaAsignatura(listaAsignaturas.get(i).getId()), i));
                i++;
            }

            BarDataSet barDataSet = new BarDataSet(barEntries, "Asignaturas");
            if(getContext()!=null)
            barDataSet.setColors(new int[]{R.color.colorButtonBlue, R.color.colorButtonGreen, R.color.colorButtonYellow, R.color.colorButtonRed, R.color.colorBluePastel, R.color.colorGreenPastel}, getContext());

            i = 0;
            mNombreAsignaturas = new ArrayList<>();

            mNombreAsignaturasSinCortar = new ArrayList<>();
            while (i < listaAsignaturas.size()) {
                mNombreAsignaturasSinCortar.add(listaAsignaturas.get(i).getNombre_asignatura());
                String cadena = listaAsignaturas.get(i).getNombre_asignatura();
                char[] arrayChar = cadena.toCharArray();
                String NombreFinal = "";
                if (arrayChar.length > 10) {
                    int j = 0;
                    while (j < arrayChar.length) {
                        if (Character.isUpperCase(cadena.charAt(j))) {
                            NombreFinal = NombreFinal + cadena.charAt(j);
                        }
                        j++;
                    }
                } else {
                    NombreFinal = cadena;
                }
                mNombreAsignaturas.add(NombreFinal);
                i++;
            }


            BarData theData = new BarData(mNombreAsignaturas, barDataSet);



            mBarChart.getAxisLeft().setAxisMaxValue(10);
            mBarChart.getAxisLeft().setAxisMinValue(0);
            mBarChart.getAxisLeft().setGranularity(1f);
            mBarChart.getAxisLeft().setGranularityEnabled(true);
            mBarChart.getAxisRight().setEnabled(false);
            mBarChart.setScaleEnabled(false);
            mBarChart.setData(theData);
            mBarChart.getBarData().setValueTextSize(8f);
            mBarChart.setDrawGridBackground(true);
            mBarChart.setDrawBorders(true);
            mBarChart.setBorderColor(Color.WHITE);
            mBarChart.setGridBackgroundColor(Color.WHITE);
            mBarChart.refreshDrawableState();
            mBarChart.setDescriptionPosition(0, 0);
        } else
            mBarChart.setNoDataText("Añade asignaturas primero para mostrar el gráfico");
    }

    /**
     * Método encargado de calcular la media de una asignatura a partir de un id
     * @param id    Id de la asignatura
     * @return  media calculada de la asignatura
     */
    public float MediaAsignatura(long id) {
        List<Tarea> tarea = DaoApp.getTareaDao().loadAll();
        String t_tarea = "";
        Float nota_tarea;
        Float nota_Actividad = 0f;
        Float nota_Practicas = 0f;
        Float nota_Teoria = 0f;
        int contadorPractica = 0;
        int contadorTeoria = 0;
        int contadorActividad = 0;
        int i = 0;
        while (i < tarea.size()) {
            if (tarea.get(i).getId_tarea() == id) {
                t_tarea = tarea.get(i).getTipo_tarea();
                nota_tarea = tarea.get(i).getNota_tarea();
                switch (t_tarea) {
                    case "Proyecto":
                        contadorPractica++;
                        nota_Practicas = (nota_tarea * 0.4f) + nota_Practicas;
                        break;
                    case "Examen_Teorico":
                        contadorTeoria++;
                        nota_Teoria = (nota_tarea * 0.5f) + nota_Teoria;
                        break;
                    case "Actividad":
                        contadorActividad++;
                        nota_Actividad = (nota_tarea * 0.1f) + nota_Actividad;
                        break;
                    case "Examen_Practico":
                        contadorPractica++;
                        nota_Practicas = (nota_tarea * 0.4f) + nota_Practicas;
                        break;
                }
            }
            i++;
        }
        if (contadorActividad != 0)
            nota_Actividad = nota_Actividad / contadorActividad;
        if (contadorPractica != 0)
            nota_Practicas = nota_Practicas / contadorPractica;
        if (contadorTeoria != 0)
            nota_Teoria = nota_Teoria / contadorTeoria;

        return nota_Actividad + nota_Practicas + nota_Teoria;
    }

    @Override
    public void onValueSelected(Entry e, int dataSetIndex, Highlight h) {

    }

    @Override
    public void onNothingSelected() {

    }
}
