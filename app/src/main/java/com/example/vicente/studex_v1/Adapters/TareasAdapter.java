package com.example.vicente.studex_v1.Adapters;

import android.content.Context;
import android.provider.ContactsContract;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

import com.example.vicente.studex_v1.DaoApp;
import com.example.vicente.studex_v1.R;

import java.util.ArrayList;
import java.util.List;

import de.greenrobot.dao.query.DeleteQuery;
import de.greenrobot.dao.query.QueryBuilder;
import io.dflabs.greendaosample.dao.AsignaturaMatriculada;
import io.dflabs.greendaosample.dao.Tarea;
import io.dflabs.greendaosample.dao.TareaDao;


/**
 * Clasea creada para utilizarla como adaptador de las tareas.
 */
public class TareasAdapter extends ArrayAdapter<Tarea> {

    /*** Lista con las tareas.*/
    private List<Tarea> tareas;

    /*** Atributos de los elementos que apareceran dentro de cada elemento de la ListView*/
    private  TextView nota;
    private TextView nombre_tarea;
    private TextView tipo_tarea;
    private ImageButton buttonDelete;

    /**
     * Metodo Constructor
     * @param context
     * @param objects Lista de tareas
     */
    public TareasAdapter(Context context,List<Tarea> objects) {
        super(context,0, objects);
        this.tareas=objects;
    }

    @Override
    public View getView(final int position, View convertView, final ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) getContext()
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (null == convertView) {
            convertView = inflater.inflate(
                    R.layout.list_item_tarea,
                    parent,
                    false);
        }

        nota = (TextView) convertView.findViewById(R.id.nota_tarea);
        nombre_tarea = (TextView) convertView.findViewById(R.id.nombre_tarea);
        tipo_tarea = (TextView) convertView.findViewById(R.id.tipo_tarea);
        buttonDelete =(ImageButton)convertView.findViewById(R.id.deleted_note);

        buttonDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                eliminarTarea(tareas.get(position).getNombre_tarea());
                tareas.remove(position);
                notifyDataSetChanged();
            }
        });
        Tarea tarea = tareas.get(position);

        nombre_tarea.setText(tarea.getNombre_tarea());
        nota.setText(tarea.getNota_tarea().toString());
        tipo_tarea.setText(tarea.getTipo_tarea());

        return convertView;
    }

    /**
     * Metodo que elimina una tarea dada de la base de datos.
     * @param tarea nombre de la tarea a eliminar.
     */
    public void eliminarTarea(String tarea){
        QueryBuilder qb = DaoApp.getTareaDao().queryBuilder().where(TareaDao.Properties.Nombre_tarea.eq(tarea));
        DeleteQuery<AsignaturaMatriculada> dq=qb.buildDelete();
        dq.executeDeleteWithoutDetachingEntities();

    }
}

