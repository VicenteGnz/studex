package com.example.vicente.studex_v1.Adapters;

import android.view.View;

/**
 * Interfaz utilizada para el click de la recycler
 */
public interface ItemClickListener {
    void onItemClick(View v, int pos);

}
