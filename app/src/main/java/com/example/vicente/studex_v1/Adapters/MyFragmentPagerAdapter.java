package com.example.vicente.studex_v1.Adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.example.vicente.studex_v1.Fragments.MisAsignaturas;
import com.example.vicente.studex_v1.Fragments.Eventos;
import com.example.vicente.studex_v1.Fragments.Resumen;

/**
 * Created by Vicente on 06/11/2016.
 */

public class MyFragmentPagerAdapter extends FragmentStatePagerAdapter {
    /*** Variable utilizada para almacenar los nombres de las pestañas que tendrá la aplicación*/
    private String[] mTabTitles;
    /*** Variable usada para guardar la instancia del último fragment creado.*/
    private Resumen lastRes;

    /**
     * Constructor del Adaptador de fragments
     * @param fm
     * @param mTabTitles Titulos de los Fragment
     */
    public MyFragmentPagerAdapter(FragmentManager fm,String[] mTabTitles) {
        super(fm);
        this.mTabTitles = mTabTitles;
    }

    @Override
    public Fragment getItem(int position) {
        switch(position) {
            case 0:
                lastRes = new Resumen();
                return lastRes;
            case 1:
                return new MisAsignaturas();
            case 2:
                return new Eventos();
            default:
                return null;
        }
    }


    @Override
    public int getCount() {
        return this.mTabTitles.length;
    }

    public void update() {
        if(lastRes!=null)
        lastRes.reloadChart();
    }
    public CharSequence getPageTitle(int position){
        return this.mTabTitles[position];
    }


}
