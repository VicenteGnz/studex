package com.example.vicente.studex_v1.Fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;

import com.example.vicente.studex_v1.Adapters.AdapterAsignaturas;
import com.example.vicente.studex_v1.Adapters.ItemClickListener;
import com.example.vicente.studex_v1.Asignatura_main;
import com.example.vicente.studex_v1.DaoApp;
import com.example.vicente.studex_v1.R;

import java.util.ArrayList;
import java.util.List;

import io.dflabs.greendaosample.dao.Asignatura;
import io.dflabs.greendaosample.dao.AsignaturaDao;
import io.dflabs.greendaosample.dao.AsignaturaMatriculada;
import io.dflabs.greendaosample.dao.AsignaturaMatriculadaDao;


/**
 * Created by Vicente on 06/11/2016.
 */
public class MisAsignaturas extends Fragment  {
    /**
     * Atributos del Fragment MisAsignturas
     */
    private Spinner mSpinner;
    private ArrayList<String> mNombreAsignaturas=new ArrayList<>();
    private View v;
    private List<AsignaturaMatriculada> mListaAsignatura=new ArrayList<>();
    private ArrayList<String> array_sort = new ArrayList<String>();
    private ArrayAdapter<String> mLTRadapter;
    private EditText et;
    private RecyclerView rv;
    private AdapterAsignaturas mAdaptador;
    private Listener mListener;

    /**
     * Interfaz utilizada para comunicarme con la actividad Principal
     */
    public interface Listener {
        void send();
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        v = inflater.inflate(R.layout.mis_asignaturas, container, false);
        mSpinner = (Spinner) v.findViewById(R.id.list_subject);
        mListener = (Listener) getActivity();

        mostrarRecyclerView();

        mAdaptador=new AdapterAsignaturas(this.getActivity(), new ItemClickListener() {
            @Override
            public void onItemClick(View v, int pos) {
                ClickOnRecycler(pos);
            }
        }, mListaAsignatura);


        retrieve();
        mostrarSpinner();
        gestionBuscador();

        ImageButton button=(ImageButton) v.findViewById(R.id.button_addSubject);
        button.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                save( mSpinner.getSelectedItem().toString());
            }
        });

        return v;
    }

    /**
     * Método encargado de gestionar el buscador de asignaturas, en el cual hace un filtrado de dichas asignaturas segun una cadena escrita
     */
    private void gestionBuscador() {

        et =(EditText) v.findViewById(R.id.text_filtro);
        et.clearFocus();
        et.addTextChangedListener(new TextWatcher() {
            int textlength = 0;
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                mSpinner.setAdapter(new ArrayAdapter<>(getActivity(),R.layout.spinner_item, mNombreAsignaturas));

            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                mSpinner.setAdapter(new ArrayAdapter<>(getActivity(),R.layout.spinner_item, mNombreAsignaturas));

            }
            @Override
            public void afterTextChanged(Editable s) {
                textlength = et.getText().length();
                array_sort.clear();
                for (int i = 0; i < mNombreAsignaturas.size(); i++) {
                    if (textlength <= mNombreAsignaturas.get(i).length()) {
                        if (et.getText().toString().equalsIgnoreCase((String) mNombreAsignaturas.get(i).subSequence(0, textlength))) {
                            if(!array_sort.contains(mNombreAsignaturas.get(i)))
                                array_sort.add(mNombreAsignaturas.get(i));
                        }
                    }
                }
                mSpinner.setAdapter(new ArrayAdapter<String>(getActivity(),R.layout.spinner_item, array_sort));
            }


        });
    }

    /**
     * Método que muestra la RecyclerView en el Fragment
     */
    private void mostrarRecyclerView() {
        rv= (RecyclerView) v.findViewById(R.id.listaRecicler);

        LinearLayoutManager llm = new LinearLayoutManager(this.getActivity());
        llm.setOrientation(LinearLayoutManager.VERTICAL);

        rv.setLayoutManager(llm);
        rv.setItemAnimator(new DefaultItemAnimator());

        retrieve();
    }

    /**
     * Método encargado de gestionar el click del RecyclerView
     * @param pos posicion del recycler
     */
    private void ClickOnRecycler(int pos) {

        Intent in=new Intent(getContext(), Asignatura_main.class);
        List<AsignaturaMatriculada> listaAsignaturasMatriculada=DaoApp.getAsigMatDao().loadAll();

        long[] ids=new long[listaAsignaturasMatriculada.size()];
        ids[0]=listaAsignaturasMatriculada.get(pos).getId();
        listaAsignaturasMatriculada.remove(pos);

        int i=1;
        int k=0;
        while(k<listaAsignaturasMatriculada.size()){
                ids[i] = listaAsignaturasMatriculada.get(k).getId();
            i++;
            k++;
            }

        Bundle bundle = new Bundle();
        bundle.putLongArray("ids",ids);
        in.putExtras(bundle);
        startActivity(in);
    }

    /**
     * Método encargado de mostrar el Spinner del Fragment Mis Asignaturas.
     */
    public void mostrarSpinner(){


        List<Asignatura> ListaNombreAsignaturas=DaoApp.getAsigDao().loadAll();
        int i=0;
        mNombreAsignaturas.clear();
        while(i<ListaNombreAsignaturas.size()){
            mNombreAsignaturas.add(ListaNombreAsignaturas.get(i).getNombre_asignatura());
            i++;
        }
        mLTRadapter = new ArrayAdapter<>(this.getActivity(), R.layout.spinner_item, mNombreAsignaturas);
        mLTRadapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
        mSpinner.setAdapter(mLTRadapter);
    }

    /**
     * Método que dada una asignatura, se le asigna la foto y el enlace del programa de la asignatura correspondiente.
     * @param asignatura
     */
    public void ponerEspecialidadesAsignatura(AsignaturaMatriculada asignatura){
        switch(asignatura.getNombre_asignatura()){
            case "Desarrollo de Programas":
                asignatura.setFoto(R.drawable.java);
                asignatura.setLinkProgramaAsignatura("http://www.unex.es/conoce-la-uex/centros/epcc/informacion-academica/programas-asignaturas/curso-2016-17/plan1632/501275.pdf");
                break;
            case "Analisis y Diseño de Algoritmos":
                asignatura.setFoto(R.drawable.knowledge);
                asignatura.setLinkProgramaAsignatura("http://www.unex.es/conoce-la-uex/centros/epcc/informacion-academica/programas-asignaturas/curso-2016-17/plan1632/501273.pdf");
                break;
            case "Programación en Internet":
                asignatura.setFoto(R.drawable.jsonfile);
                asignatura.setLinkProgramaAsignatura("http://www.unex.es/conoce-la-uex/centros/epcc/informacion-academica/programas-asignaturas/curso-2016-17/plan1632/501295.pdf");
                break;
            case "Teoría de Lenguajes":
                asignatura.setFoto(R.drawable.binarycode);
                asignatura.setLinkProgramaAsignatura("http://www.unex.es/conoce-la-uex/centros/epcc/informacion-academica/programas-asignaturas/curso-2016-17/plan1632/501321.pdf");
                break;
            case "Arquitecturas Software Para Entornos Empresariales":
                asignatura.setFoto(R.drawable.android);
                asignatura.setLinkProgramaAsignatura("http://www.unex.es/conoce-la-uex/centros/epcc/informacion-academica/programas-asignaturas/curso-2016-17/plan1632/501315.pdf");
                break;
            case "Diseño e Interacción de Sistemas Inteligentes":
                asignatura.setFoto(R.drawable.mind);
                asignatura.setLinkProgramaAsignatura("http://www.unex.es/conoce-la-uex/centros/epcc/informacion-academica/programas-asignaturas/curso-2016-17/plan1632/501282.pdf");
                break;
            case "Fisica":
                asignatura.setFoto(R.drawable.science);
                asignatura.setLinkProgramaAsignatura("http://www.unex.es/conoce-la-uex/centros/epcc/informacion-academica/programas-asignaturas/curso-2016-17/plan1632/501268.pdf");
                break;
            case "Calculo":
                asignatura.setFoto(R.drawable.graphic);
                asignatura.setLinkProgramaAsignatura("http://www.unex.es/conoce-la-uex/centros/epcc/informacion-academica/programas-asignaturas/curso-2016-17/plan1632/502381.pdf");
                break;
            case "Algebra":
                asignatura.setFoto(R.drawable.coordinates);
                asignatura.setLinkProgramaAsignatura("http://www.unex.es/conoce-la-uex/centros/epcc/informacion-academica/programas-asignaturas/curso-2016-17/plan1632/502382.pdf");
                break;
            default:
                asignatura.setFoto(R.drawable.java);
                asignatura.setLinkProgramaAsignatura("http://www.unex.es/conoce-la-uex/centros/epcc/titulaciones/info/asignaturas?id=1632");
        }

    }

    /**
     * Método que actualiza el RecyclerView
     */
    private void retrieve()
    {
        List<AsignaturaMatriculada> listAsigMat=DaoApp.getAsigMatDao().loadAll();
        mListaAsignatura.clear();
        int i=0;
        while(i<listAsigMat.size()) {
            ponerEspecialidadesAsignatura(listAsigMat.get(i));
            mListaAsignatura.add(listAsigMat.get(i));
            i++;
        }
        rv.setAdapter(mAdaptador);
        sendInfoRetrieve();
    }

    /**
     * Metodo que comprueba en la Base de datos si existe dicha asignatura a partir de un Nombre dado.
     * @param Nombre nombre de la asignatura
     * @return boolean si existe o no
     */
    private boolean existeAsignaturaMatriculada(String Nombre){
        List<AsignaturaMatriculada> asignatura = DaoApp.getAsigMatDao().queryBuilder().where(AsignaturaMatriculadaDao.Properties.Nombre_asignatura.eq(Nombre)).build().list();
        if(asignatura.size()<1)
            return false;
        else
            return true;
    }

    /**
     * Método que almacena en la base de datos una Asignatura a partir de un nombre
     * @param name
     */
    private void save(String name)
    {

        List<Asignatura> asignatura= DaoApp.getAsigDao().queryBuilder().where(AsignaturaDao.Properties.Nombre_asignatura.eq(name)).build().list();
        if(!asignatura.isEmpty()) {
            AsignaturaMatriculada a = new AsignaturaMatriculada(asignatura.get(0).getId(), asignatura.get(0).getNombre_asignatura(), 60, 30, 10);
            ponerEspecialidadesAsignatura(a);
            if(!existeAsignaturaMatriculada(a.getNombre_asignatura()))
            DaoApp.getAsigMatDao().insertInTx(a);
        }
        retrieve();
    }

    /**
     * Metodo implementado de la interfaz
     */
    public void sendInfoRetrieve(){
        mListener.send();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        mostrarSpinner();
        retrieve();
    }

    @Override
    public void onResume() {
        super.onResume();
        mostrarSpinner();
        retrieve();

    }

}