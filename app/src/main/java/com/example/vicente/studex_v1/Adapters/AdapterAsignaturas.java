package com.example.vicente.studex_v1.Adapters;

import android.content.Context;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import com.example.vicente.studex_v1.DaoApp;
import com.example.vicente.studex_v1.Fragments.MisAsignaturas;
import com.example.vicente.studex_v1.R;

import java.util.ArrayList;
import java.util.List;

import de.greenrobot.dao.query.DeleteQuery;
import de.greenrobot.dao.query.QueryBuilder;
import io.dflabs.greendaosample.dao.AsignaturaMatriculada;
import io.dflabs.greendaosample.dao.AsignaturaMatriculadaDao;
import io.dflabs.greendaosample.dao.TareaDao;

/**
 * Created by Vicente on 07/11/2016.
 */

public class AdapterAsignaturas extends RecyclerView.Adapter<AsignaturaViewHolder> {
    /*** Instancia de la interfaz*/
    ListenerToDeleteSubject mListener;

    /*** Interfaz utilizada para poder actualizar un Fragment.*/
    public interface ListenerToDeleteSubject{
        void sendUdapte();
    }

    /*** Lista de las asignaturasMatriculadas*/
    protected List<AsignaturaMatriculada> items;

    /*** Contexto*/
    Context c;


    /*** Listener creado para poder clickear en las asignaturas*/
    ItemClickListener listener;

    /**
     * Metodo en el que se le asigna un elemento ItemClickListener
     * @param listener
     *                  .listener utilizado para hacer click en la asignatura.
     */
    public void setListener(ItemClickListener listener) {
        this.listener = listener;
    }

    /**
     * Constructor del Adaptqador de asginaturas.
     * @param ctx
     *             .contexto de la actividad
     * @param listener
     *              .Listener a utilizar
     * @param _items
     *              .Lista de objetos que vamos a tener en el adapter
     */
    public AdapterAsignaturas(Context ctx, ItemClickListener listener, List<AsignaturaMatriculada> _items){
        mListener =(ListenerToDeleteSubject) ctx;
        this.items=_items;
        this.c=ctx;
        setListener(listener);
    }

    /**
     * Metodo que crea la ViewHolder
     * @param parent
     * @param viewType
     * @return
     */
    public AsignaturaViewHolder onCreateViewHolder(ViewGroup parent,int viewType){
        View v=LayoutInflater.from(parent.getContext()).inflate(R.layout.itemlista,null);
        AsignaturaViewHolder holder=new AsignaturaViewHolder(v);
        return holder;
    }

    /**
     *  Metodo que muestra la información e una posicion dada
     * @param holder
     * @param position
     */
    public void onBindViewHolder(final AsignaturaViewHolder  holder, final int position){
        holder.nomAsignatura.setText(items.get(position).getNombre_asignatura());
        holder.imgAsignatura.setImageResource(items.get(position).getFoto());

        if (listener != null)
            holder.setItemClickListener(listener);


        holder.button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                eliminarAsignatura(items.get(position).getId());
                items.remove(position);
                notifyDataSetChanged();
            }
        });

    }

    /**
     * Metodo que elimina una asignatura de la base de datos
     * @param id
     *          .id de la asignatura que se quiere eliminar
     */
    public void eliminarAsignatura(long id){
        QueryBuilder qb = DaoApp.getAsigMatDao().queryBuilder().where(AsignaturaMatriculadaDao.Properties.Id.eq(id));
        DeleteQuery<AsignaturaMatriculada> dq=qb.buildDelete();
        dq.executeDeleteWithoutDetachingEntities();

        QueryBuilder qb2 = DaoApp.getTareaDao().queryBuilder().where(TareaDao.Properties.Id_tarea.eq(id));
        DeleteQuery<AsignaturaMatriculada> dq2=qb2.buildDelete();
        dq2.executeDeleteWithoutDetachingEntities();
        mListener.sendUdapte();

    }

    @Override
    public long getItemId(int position) {
        return items.get(position).getId();
    }

    @Override
    public int getItemCount() {
        return items.size();
    }


}
