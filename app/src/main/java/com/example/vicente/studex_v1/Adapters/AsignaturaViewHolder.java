package com.example.vicente.studex_v1.Adapters;

import android.provider.ContactsContract;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.vicente.studex_v1.DaoApp;
import com.example.vicente.studex_v1.R;

import de.greenrobot.dao.query.DeleteQuery;
import de.greenrobot.dao.query.QueryBuilder;
import io.dflabs.greendaosample.dao.AsignaturaMatriculada;
import io.dflabs.greendaosample.dao.AsignaturaMatriculadaDao;
import io.dflabs.greendaosample.dao.TareaDao;

/**
 * Created by Vicente on 14/11/2016.
 */
public  class AsignaturaViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

    /*** Atributos utilizados para instanciar los elementos que apareceran en la Recycler*/
    ImageView imgAsignatura;
    TextView nomAsignatura;
    ImageButton button;
    ItemClickListener itemClickListener;

    /**
     * Constructor de la ViewHolder
     * @param itemView
     */
    public AsignaturaViewHolder(View itemView){
        super(itemView);
        nomAsignatura=(TextView) itemView.findViewById(R.id.text_Asignatura);
        imgAsignatura=(ImageView) itemView.findViewById((R.id.imagen_Asignatura));
        button =(ImageButton) itemView.findViewById(R.id.button_deleteSub);

        itemView.setOnClickListener(this);

    }
    public void setItemClickListener(ItemClickListener ic)
    {
        this.itemClickListener=ic;
    }

    public void onClick(View v) {
        this.itemClickListener.onItemClick(v,getLayoutPosition());
    }


}